// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC3ivZKA8OTy3mTMDN9-FNgWvix3tOIHt8",
    authDomain: "materail-angular-designapp.firebaseapp.com",
    databaseURL: "https://materail-angular-designapp.firebaseio.com",
    projectId: "materail-angular-designapp",
    storageBucket: "materail-angular-designapp.appspot.com",
    messagingSenderId: "92201064365"
    }};

 

    



/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
