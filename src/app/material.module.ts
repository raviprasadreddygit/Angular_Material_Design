   
// All The   material  Component modules we need to add into  this Module


import { NgModule } from '@angular/core';


import {MatFormFieldModule,
         MatInputModule,
         MatNativeDateModule,
         MatButtonModule,
         MatSelectModule,
         MatCheckboxModule,
         MatSidenavModule,
         MatToolbarModule,
         MatTableModule,
         MatCardModule,

} from '@angular/material';



import {MatDatepickerModule,
  
} from '@angular/material/datepicker';




@NgModule({
  declarations: [
  ],
  imports: [MatDatepickerModule,
            MatFormFieldModule,
            MatInputModule,
            MatNativeDateModule,
            MatButtonModule,
            MatSelectModule,
            MatCheckboxModule,
            MatSidenavModule ,
            MatToolbarModule  ,
            MatTableModule  ,
            MatCardModule

          ],
  //we need to use these modules into another App.module we need to Exports  these modiules .
  exports :[MatDatepickerModule,
            MatFormFieldModule,
            MatInputModule,
            MatNativeDateModule,
            MatButtonModule,
            MatSelectModule,
            MatCheckboxModule,
            MatSidenavModule ,
            MatToolbarModule ,
            MatTableModule,
            MatCardModule

           ],
  providers: [],
})
export class MaterialModule { }
