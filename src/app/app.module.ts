import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';



import {FlexLayoutModule} from '@angular/flex-layout';


import { AppComponent } from './app.component';




import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { TrainingComponent } from './training/training.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AppRoutingModule } from './app.routing.module';


import { AngularFireModule } from 'angularfire2';


import { AngularFirestoreModule } from 'angularfire2/firestore';


import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { TrainingService } from './training/training.service';
import { AppService } from './app.service';
 

import { DataTablesModule } from 'angular-datatables';


import { HttpModule } from '@angular/http';



import {HttpClientModule} from '@angular/common/http';

import { environment } from '../environments/environment';
import { SigninService } from './auth/signin/signin.service';


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    TrainingComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    //angular firebase 
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    HttpModule,
    
    FlexLayoutModule,
    ReactiveFormsModule,
    DataTablesModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [TrainingService,SigninService, AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
